package note.utils;

import note.utils.Constants;

public class ClasaException extends Exception {
    public ClasaException(Integer i) {
        super(i.toString());
    }

    public ClasaException(String s) {
        super(s);
    }
}
