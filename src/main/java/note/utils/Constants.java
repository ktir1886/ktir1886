package note.utils;

public class Constants {
	public static Integer minNota = 1;
	public static Integer maxNota = 10;
	public static Integer minNrmatricol = 1;
	public static Integer maxNrmatricol = 1000;
	public static String invalidNota = "Nota introdusa nu este corecta";
	public static String invalidMateria = "Lungimea materiei este invalida";
	public static String invalidNrmatricol = "Numarul matricol introdus nu este corect";
	public static String emptyRepository = "Nu exista date";
}
