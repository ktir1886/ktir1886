package note.model;

public class Corigent {
	private Elev numeElev;
	private int nrMaterii;
	/**
	 * @return the numeElev
	 */
	
	public Corigent(Elev numeElev, int nrMaterii) {
		this.numeElev = numeElev;
		this.nrMaterii = nrMaterii;
	}
	
	
	public Elev getNumeElev() {
		return numeElev;
	}
	/**
	 * @param numeElev the numeElev to set
	 */
	public void setNumeElev(Elev numeElev) {
		this.numeElev = numeElev;
	}
	/**
	 * @return the nrMaterii
	 */
	public int getNrMaterii() {
		return nrMaterii;
	}
	/**
	 * @param nrMaterii the nrMaterii to set
	 */
	public void setNrMaterii(int nrMaterii) {
		this.nrMaterii = nrMaterii;
	}

	public String toString() {
		return numeElev + " -> " + nrMaterii;
	}
}
